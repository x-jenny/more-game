import pygame
from gamectrl import GameController, EventHandler, GameWindow

startpic = pygame.image.load('icon.jpeg')
startpic = pygame.transform.scale(startpic, (100, 100))
exitpic = pygame.image.load('exit.jpg')
exitpic = pygame.transform.scale(exitpic, (100, 100))

class MainMenu:
	def __init__(self, mediator):
		self.running = True
		self.buttons = pygame.sprite.Group()
		self.mediator = mediator
		self.mediator.add(self, pygame.QUIT)
	
	def start(self):
		self.buttons.add(NormalButton(self.mediator))
		self.buttons.add(ExitButton(self.mediator))
	
	def notify(self, event):
		self.running = False


class Button(pygame.sprite.Sprite):
	def __init__(self, image, mediator):
		pygame.sprite.Sprite.__init__(self)
		self.image = image
		self.rect = self.image.get_rect()
		self.mediator = mediator
		self.mediator.add(self, pygame.MOUSEBUTTONUP)
	
	def post(self):
		pass
	
	def notify(self, event):
		if self.rect.collidepoint(pygame.mouse.get_pos()):
			self.post()

class NormalButton(Button):
	def __init__(self, mediator):
		super().__init__(startpic, mediator)
		self.rect.center = 720/3, 460/2
		
	def post(self):
		print("Not connected.")

class ExitButton(Button):
	def __init__(self, mediator):
		super().__init__(exitpic, mediator)
		self.rect.center = 720/3*2, 460/2
		
	def post(self):
		pygame.event.post(pygame.event.Event(pygame.QUIT, {}))


gossipgirl = EventHandler()
main = MainMenu(gossipgirl)
view = GameWindow((720, 460))

view.add_group(main.buttons)

main.start()
view.draw()

while main.running:
	
	gossipgirl.sort_events()
	
