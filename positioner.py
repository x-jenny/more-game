import pygame, sys

class Positioner:
	def __init__(self):
		self.lanes = 4
		self.squares = 6
		self.tile_side = 100
		self.card_size = "This decides the card_bar"
		self.card_bar = 120
		self.status_bar = 60
		self.width = self.card_bar + self.squares * self.tile_side
		self.height = self.status_bar + self.lanes * self.tile_side
	
	def lane_center(self, lane):
		return int(self.status_bar + (lane - 0.5) * self.tile_side)
		
	def lane_top(self, lane):
		return int(self.status_bar + (lane - 1) * self.tile_side)
		
	def lane_bottom(self, lane):
		return int(self.status_bar + lane * self.tile_side)
	
	def square_center(self, square):
		return int(self.card_bar + (square - 0.5) * self.tile_side)
	
	def square_left(self, square):
		return int(self.card_bar + (square - 1) * self.tile_side)
	
	def square_right(self, square):
		return int(self.card_bar + square * self.tile_side)
	
	def random_lane(self, x):
		return pos
		
	def random_square(self, y):
		return pos
	
	def random_tile(self):
		pass
	
	def tile_center(self, lane, square):
		return self.square_center(square), self.lane_center(lane)
	
	def tile_top_left(self, lane, square):
		return self.square_left(square), self.lane_top(lane)
	
	def return_tile(self, point):
		x, y = point
		lane = (y - self.status_bar) // self.tile_side + 1
		square = (x - self.card_bar) // self.tile_side + 1
		return lane, square
	
	def off_screen(self, rect):
		pass 


pea_org = pygame.image.load('pea.jpg')
pea = pygame.transform.scale(pea_org, (100,100))

star = pygame.image.load('star.png')

daddy = Positioner()
print(daddy.width, daddy.height)



screen = pygame.display.set_mode((daddy.width, daddy.height))
bgcolor = 250, 250, 250


while True:
	for event in pygame.event.get():
			if event.type == pygame.QUIT:
				sys.exit()
			elif event.type == pygame.MOUSEMOTION:
				lane, square = daddy.return_tile(event.pos)
				pos = daddy.tile_top_left(lane,square)
				screen.fill(bgcolor)
				screen.blit(star, pos)
				pygame.display.flip()
