import pygame, gw, sys
from gameobj import *
from gamectrl import GameController






# Functions

# Creating things

# Creating defender
def create_def(x, y):
	unit = Defender(tileCenter(y, x))
	unit.add(defenders)
	board.place(unit, x, y)
	weapons.add(Weapon(tileTopLeft(y, x), tile_size, tileCenter(y, x)))

# Create attacker
def create_att(x, y=6):
	attackers.add(Attacker(tileCenter(y, x)))

# Create bullet
def create_bull(position):
	bullets.add(Bullet(position))


# Game rules

# Weapon detect attackers
def enemy_detection():
	for weapon in weapons:
		weapon.detect(attackers)

# Bullet damage attacker
def x():
	hits = pygame.sprite.groupcollide(bullets, attackers, False, False)
	for bull, atts in hits.items():
		atts[0].hit(bull.damage)
		bull.die()




# Temporary Functions

# To move all existing attackers. In future timer will trigger move.
def move_atts():
	for obj in attackers:
		obj.walk()


def move_bullets():
	for bull in bullets:
		bull.move()
		print(bull.rect.center)


def coord(group):
	for unit in group:
		print(unit.rect)

# Creating objects
board = Board(h, w, tile_size)

create_def(0,1)
create_def(3,2)
create_def(2,0)

create_att(0)
create_att(1)
create_att(2)


# Creating game window
screen = gw.gameWindow((w*tile_size, h*tile_size))
screen.draw([defenders, attackers])

while True:
	
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			sys.exit()
		elif event.type == pygame.MOUSEBUTTONUP:
			move_atts()
			move_bullets()
 			screen.draw([defenders, attackers, bullets])
			enemy_detection()
			x()
			
	
