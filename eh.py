import sys, pygame

# Can rewrite to be independent of pygame

class EventHandler:
	def __init__(self):
		self.subscribers = {pygame.QUIT:[], pygame.MOUSEBUTTONUP:[]}
		
	def add(self, subscriber, EvType):
		if EvType in self.subscribers:
			self.subscribers[EvType].append(subscriber)
		
	def sort_events(self):
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				sys.exit()
			elif event.type == pygame.MOUSEBUTTONUP:
				for listener in self.mouseClick:
					listener.notify()
