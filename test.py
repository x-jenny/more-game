import pygame

img_snail = pygame.image.load('snail.gif')
snailpic = pygame.transform.scale(img_snail, (90,90))

class Lawn:
	def __init__(self, x1, y1, dx, dy):
		self.lane = [ y1 + i*dy for i in range(4)]
		self.col = [ x1 + i*dx for i in range(6)]
		
	def tile(self, x, y):
		return self.lane[x-1], self.col[y-1]

class Square(pygame.sprite.Sprite):
	def __init__(self, center):
		pygame.sprite.Sprite.__init__(self)
		self.image = img_square
		self.rect = self.image.get_rect()
		self.rect.center = center

class Snail(pygame.sprite.Sprite):
	def __init__(self, startpoint, move):
		pygame.sprite.Sprite.__init__(self)
		self.image = snailpic
		self.rect = self.image.get_rect()
		self.rect.center = startpoint
		self.move = move
		
	def update(self):
		self.walk()
		
	def walk(self):
		self.rect = self.move.toRight(self.rect, 'fast')
	
def newSprite(spriteType, tile):
	gameObject = spriteType(lawn.tile(tile))
	if isinstance(newSprite, Snail):
		snailGroup.append(gameObject)
		
class Move:
	
	def __init__(self):
		self.count = 1
		self.speedTypes = []
	
	def speedControll(self):
		newTypes = []
		
		if self.count == 48:
			self.count = 1
		else:
			self.count += 1
		
		if self.count % 2 == 0:
			newTypes.append('vfast')
		if self.count % 4 == 0:
			newTypes.append('fast')
		if self.count % 8 == 0:
			newTypes.append('normal')
		if self.count % 12 == 0:
			newTypes.append('slow')
		if self.count % 16 == 0:
			newTypes.append('vslow')
		
		self.speedTypes[:] = newTypes[:]
	
	def toLeft(self, rect, speed):
		if speed in self.speedTypes:
			rect.x -= 1
		return rect
		
	def toRight(self, rect, speed):
		if speed in self.speedTypes:
			rect.x += 1
		return rect
