import pygame

# gameWindow parameters
desktop_icon = pygame.image.load('icon.jpeg')
bgcolor = 20,20,20


class gameWindow:
	
	def __init__(self, size):
		pygame.display.set_icon(desktop_icon)
		pygame.display.set_caption('Game under construction')
		self.screen = pygame.display.set_mode(size)
		
	def draw(self, groups=[]):		# Maybe become draw game and add method draw start screen
		for group in groups:
			group.update()
			
		self.screen.fill(bgcolor) # blit background image in future
		
		for group in groups:
			group.draw(self.screen)
		
		pygame.display.flip()
