### Testing pygames transparency function


import pygame

ballpic = pygame.image.load("ball.gif")
bgcolor = 20, 20, 20
size = width, height = 340, 280
alpha = 0

pygame.init()
screen = pygame.display.set_mode(size)

class dimmer(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		self.alpha = 10
		self.image = ballpic
		self.image.set_alpha(self.alpha)
		self.rect = self.image.get_rect()
		
	def update(self):
		self.alpha += 10
		print(self.alpha)
		self.image.set_alpha(self.alpha)
		
ball = dimmer()
theBall = pygame.sprite.Group(ball)

while True:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			quit()
		elif event.type == pygame.MOUSEBUTTONUP:
			theBall.update()

	screen.fill(bgcolor)
	theBall.draw(screen)
	pygame.display.flip()
