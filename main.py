import pygame, sys
import gw

img_square = pygame.image.load('lawn.jpg')
img_snail = pygame.image.load('snail.gif')
leftsnail = pygame.transform.scale(img_snail, (90,90))
rightsnail = pygame.transform.flip(leftsnail, True, False)





# Game objects
class Lawn:
	def __init__(self, x1, y1, dx, dy):
		self.lane = [ y1 + i*dy for i in range(4)]
		self.col = [ x1 + i*dx for i in range(6)]
		
	def tile(self, x, y):
		return self.col[y-1], self.lane[x-1]

class Square(pygame.sprite.Sprite):
	def __init__(self, center):
		pygame.sprite.Sprite.__init__(self)
		self.image = img_square
		self.rect = self.image.get_rect()
		self.rect.center = center

class Snail(pygame.sprite.Sprite):
	def __init__(self, startPoint, move):
		pygame.sprite.Sprite.__init__(self)
		self.image = rightsnail
		self.rect = self.image.get_rect()
		self.rect.center = startPoint
		self.move = move
		self.speed = 'normal'
		
	def update(self):
		self.walk()
		
	def walk(self):
		self.rect = self.move.toRight(self.rect, self.speed)
		if self.rect.right < 0 or self.rect.left > gw.width:
			self.kill()
			
		
class mirrorSnail(Snail):
	def __init__(self, startPoint, move):
		super().__init__(startPoint, move)
		self.image = leftsnail
		self.speed = 'slow'
		
	def walk(self):
		self.rect = self.move.toLeft(self.rect, self.speed)
		if self.rect.right < 0 or self.rect.left > gw.width:
			self.kill()

# My function classes	
class Move:
	
	def __init__(self):
		self.count = 1
		self.speedTypes = []
	
	def speedControll(self):
		newTypes = []
		
		if self.count == 48:
			self.count = 1
		else:
			self.count += 1
		
		if self.count % 2 == 0:
			newTypes.append('vfast')
		if self.count % 4 == 0:
			newTypes.append('fast')
		if self.count % 8 == 0:
			newTypes.append('normal')
		if self.count % 12 == 0:
			newTypes.append('slow')
		if self.count % 16 == 0:
			newTypes.append('vslow')
		
		self.speedTypes[:] = newTypes[:]
	
	def toLeft(self, rect, speed):
		if speed in self.speedTypes:
			rect.x -= 1
		return rect
		
	def toRight(self, rect, speed):
		if speed in self.speedTypes:
			rect.x += 1
		return rect
		
def newSprite(spriteType, x, y):
	gameObject = spriteType(lawn.tile(x, y), move)
	if isinstance(gameObject, Snail):
		print('was here')
		snailGroup.add(gameObject)
		
def removeSprite(sprite):
	sprite.kill()

# Instantiate lawn and move object
lawn = Lawn(90, 90, 115, 115)
move = Move()

# Create tiles on lawn
squares = []
for i in range(4):
	for j in range(6):
		squares.append(Square((lawn.col[j], lawn.lane[i])))

# Create a man
little_snail = Snail(lawn.tile(2,1), move)
another_snail = mirrorSnail(lawn.tile(3,5), move)

# Make sprite groups	
squareGroup = pygame.sprite.Group(*squares)
snailGroup = pygame.sprite.Group(little_snail, another_snail)

# Draw
screen = gw.gameWindow((800, 600))


while True:
	
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			sys.exit()
	#	elif event.type == pygame.MOUSEBUTTONUP:
	move.speedControll()
	screen.draw([squareGroup, snailGroup])
