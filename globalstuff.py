# EVENT TYPES
RELEASE = 25
ATT_RELEASE = 26
MOVE = 27
DEF_REMOVE = 28
START_GAME = 29
TICKS = 30
STOP = 31

move_interval = 100

# Images
zombie = pygame.image.load('zombie.jpg')
attpic = pygame.transform.scale(zombie, (50,100))
pea = pygame.image.load('pea.jpg')
peepic = pygame.transform.scale(pea, (100,100))
bullpic = pygame.image.load('bulletarrow.gif')
respic = pygame.image.load('mineral.gif')
defpic = pygame.image.load('star.png')
melonpic = pygame.image.load('melon.png')
startpic = pygame.image.load('start.jpg')
startpic = pygame.transform.scale(startpic, (100, 100))
exitpic = pygame.image.load('exit.jpg')
exitpic = pygame.transform.scale(exitpic, (100, 100))
menupic = pygame.image.load('menu.png')
menupic = pygame.transform.scale(menupic, (50, 50))
desktop_icon = pygame.image.load('icon.jpg')
