import pygame, random
from globalstuff import *


class Level:
	# Which parameters should be configurable and which should have methods for configuring which should be sent in as parameters?
	
	def __init__(self):
		self.lanes = 0
		self.squares = 0
		self.tile_side = 100
		self.bgImage = None
		self.resources = {}
		self.def_types = {}
		self.waves = []
		
	#	Sets the size of the game board
	def set_board_size(self, lanes, squares):
		self.lanes = lanes
		self.squares = squares
	
	def add_defender_type(self, dtype):
		self.def_types[dtype] = 1
		
	def rm_defender_type(self, dtype):
		if dtype in self.def_types:
			del self.def_types[dtype]
		
	def add_wave(self, wave):
		self.waves.append(wave)
		
	def rm_wave(self, wave):
		if wave in self.waves:
			self.waves.remove(wave)

class Wave:
	def __init__(self):
		self.timer = 'a time'
		self.enemies = {}
	
	def __repr__(self):
		return str(self.enemies)
	
	def add(self, AttackerFactory, number):
		self.enemies[AttackerFactory] = number
		
	def remove(AttackerFactory):
		if AttackerFactory in self.enemies:
			del self.enemies[AttackerFactory]
	
	def get_enemy(self):
	# 	Random comes to use when we have more types of attackers
		enemy = random.choice(list(self.enemies))
		self.enemies[enemy] -= 1
		if self.enemies[enemy] == 0:
			del self.enemies[enemy]
		if not self.enemies:
			self.finish()
		return enemy
		
	def finish(self):
		pygame.event.post(pygame.event.Event(STOP, {"message": "wave finished"}))

class Board:	# Can be rebuilt not to contain defender but only boolean.
	
	def __init__(self, lanes, squares, mediator):
		self.tiles = [[False] * squares for each in range(lanes)]
		self.mediator = mediator
		self.mediator.add(self, DEF_REMOVE)
	
	# return defender on tile or none
	def occupied(self, lane, square):
		lane -= 1
		square -= 1
		return self.tiles[lane][square]
	
	# Placing a defender on the board
	def occupy(self, occupant, lane, square):
		lane -= 1
		square -= 1
		self.tiles[lane][square] = occupant
		
	# Removing defender from board
	def set_free(self, occupant):
		for lane in self.tiles:
			if occupant in lane:
				lane[lane.index(occupant)] = False
	
	def notify(self, event):
		event.type == DEF_REMOVE:
			self.set_free(event.defender)

class MyResource:
	def __init__(self):
		self.image = None
		self.rect = None
		self.counter = 0
		
	def add(self, value):
		self.counter += value
		print(self.counter)
			
	def update(self):
		# Method to update the resouce counter on screen.
		pass

class Resource(pygame.sprite.Sprite):
	
	# Make collectable only if received mouse button up?
	
	def __init__(self, position, counter, mediator):
		pygame.sprite.Sprite.__init__(self)
		self.image = respic
		self.rect = self.image.get_rect()
		self.rect.center = position
		self.life = 'time'
		self.value = 10
		self.counter = counter
		self.mediator = mediator
		self.mediator.add(self, pygame.MOUSEBUTTONUP)
	
	def collected(self):
		self.counter.add(self.value)
		self.die()
		
	def notify(self, event):
		if self.rect.collidepoint(pygame.mouse.get_pos()):
			self.collected()
	
	def die(self):
		self.mediator.remove(self)
		self.kill()

class Attacker(pygame.sprite.Sprite):
	
	def __init__(self, position, mediator):
		pygame.sprite.Sprite.__init__(self)
		self.image = attpic
		self.size = 50, 100
		self.rect = self.image.get_rect()
		self.rect.center = position
		self.hp = 100
		self.damage = 10
		self.speed = None
		self.frequency = 15
		self.counter = 0
		self.eating = None
		self.movement = MoveAttacker(self.rect, self.speed, mediator)
		self.mediator = mediator
		self.mediator.add(self, MOVE)

	# Called when Attackers are hit
	def hit(self, damage):
		self.hp -= damage
		if self.hp <= 0:
			self.die()
	
	def movement_now(self, defenders):
		# Unecessary to reset self.movement.moving all the time?
		if pygame.sprite.spritecollideany(self, defenders):
			self.movement.moving = False
			self.eating = pygame.sprite.spritecollideany(self, defenders)
		else:
			self.movement.moving = True
			self.eating = None
				
	def eat(self):
		if self.eating:
			if self.counter == 0:
				self.eating.hit(self.damage)
				self.counter = self.frequency
			else:
				self.counter -= 1
		
	def die(self):
		self.mediator.remove(self)
		self.kill()
	
	def notify(self, event):
		self.eat()

# Factory is used for GameController to create Attackers. One Factory for each type of Attacker.
class AttackerFactory:
	def create(position, mediator):
		return Attacker(position, mediator)

class DefType(pygame.sprite.Sprite):
	def __init__(self, position, defplacer, mediator):		# Not setting center in constructor. do a set_center method.
		pygame.sprite.Sprite.__init__(self)
		self.image = peepic
		self.rect = self.image.get_rect()
		self.rect.center = position
		self.selected = False
		self.recharge = 'time'
		self.defplacer = defplacer
		self.mediator = mediator
		self.mediator.add(self, pygame.MOUSEBUTTONDOWN)
	
	def create(self):
		return Defender()
	
	def notify(self, event):
		if self.rect.collidepoint(pygame.mouse.get_pos()):
			self.defplacer.activate(self.image, self.create()) # Send in a copy of image instead, then picture alpha can be changed.

class Melon(DefType):
	def __init__(self, position, defplacer, mediator):
		super().__init__(position, defplacer, mediator)
		self.image = melonpic
		
	def create(self):
		return MelonDefender()
	
class Defender(pygame.sprite.Sprite):
	
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		self.image = peepic
		self.rect = self.image.get_rect()
		self.rect.center = -100, -100
		self.hp = 100
		self.weapon = None
	
	def set_position(self, position):
		self.rect.center = position
		
	# Called when hit by enemy bullet
	def hit(self, damage):
		self.hp -= damage
		if self.hp <= 0:
			self.die()
			
	def set_weapon(self, weapon):
		self.weapon = weapon
	
	# Remove itself from all sprite.Groups
	def die(self):
		# Notify board to remove itself from board
		self.weapon.die()
		self.kill()
		pygame.event.post(pygame.event.Event(DEF_REMOVE, {'defender': self}))

class MelonDefender(Defender):
	def __init__(self):
		super().__init__()
		self.image = melonpic
		self.hp = 150

class Weapon(pygame.sprite.Sprite):
	def __init__(self, lane, square, bullets, positioner, mediator):
	# self.area is the range where the defender starts to shoot.
		pygame.sprite.Sprite.__init__(self)
		self.area = 3
		self.positioner = positioner
		self.rect = self.positioner.range(lane, square, self.area)
		self.muzzle = self.positioner.muzzle_pos(lane, square, 70, 40)
		self.bullet = Bullet
		self.bullets = bullets
		self.frequency = 25
		self.counter = 0
		self.shooting = False
		self.mediator = mediator
		self.mediator.add(self, MOVE)
		
	def shoot(self):
		if self.shooting:
			if self.counter == 0:
				self.bullet(self.muzzle, self.bullets, self.mediator)
				self.counter = self.frequency
			else:
				self.counter -= 1
		
	def detection(self, attackerGroup):
		if pygame.sprite.spritecollideany(self, attackerGroup):
			self.shooting = True
		else:
			self.shooting = False
	
	def die(self):
		self.mediator.remove(self)
		self.kill()
	
	def notify(self, event):
		if event.type == MOVE:
			self.shoot()

class Bullet(pygame.sprite.Sprite):
	
	def __init__(self, position, bullets, mediator):
		pygame.sprite.Sprite.__init__(self)
		self.image = bullpic
		self.rect = self.image.get_rect()
		self.rect.center = position
		self.speed = 50
		self.movement = MoveBullet(self.rect, self.speed, mediator)
		self.damage = 10
		self.add(bullets)
		
	def die(self):
		self.movement.die()
		self.kill()

		# If moved out of screen, self.die(). Probably a method in GC

class House(pygame.sprite.Sprite):
	
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		self.rect = pygame.Rect(100, 60, 20, 400)
		print(self.rect)

class Move:
	# Should have been an abstract class
	def __init__(self, rect, dist, mediator, etype):
		self.rect = rect
		self.distance = dist
		self.mediator = mediator
		self.mediator.add(self, etype)
	
	def go(self):
		pass
	
	def die(self):
		self.mediator.remove(self)
	
	def notify(self, event):
		if event.type == MOVE:
			self.go()
	
class MoveAttacker(Move):
	def __init__(self, rect, dist, mediator, etype=MOVE):
		super().__init__(rect, dist, mediator, etype)
		self.moving = True
		
	def go(self):
		if self.moving:
			self.rect.move_ip(-1, 0)
	
#	def change_state(self):
#		self.moving = not self.moving

class MoveBullet(Move):
	def __init__(self, rect, dist, mediator, etype=MOVE):
		super().__init__(rect, dist, mediator, etype)
		
	def go(self):
		self.rect.move_ip(2, 0)

class Button(pygame.sprite.Sprite):
	
	def __init__(self, image, position, mediator):
		pygame.sprite.Sprite.__init__(self)
		self.image = image
		self.rect = self.image.get_rect()
		self.rect.center = position
		self.mediator = mediator
		self.mediator.add(self, pygame.MOUSEBUTTONUP)
		self.mediator.view(self)
	
	def post(self):
		pass
	
	def notify(self, event):
		if event.type == pygame.MOUSEBUTTONUP:
			if self.rect.collidepoint(pygame.mouse.get_pos()):
				print('got mouse up event', self)
				self.post()
		
class LevelButton(Button):
	
	def __init__(self, level, position, mediator):
		super().__init__(startpic, position, mediator)
		self.level = level
		
	def post(self):
		pygame.event.post(pygame.event.Event(START_GAME, {'level':self.level}))
		
class ExitButton(Button):
	
	def __init__(self, position, mediator):
		super().__init__(exitpic, position, mediator)
		
	def post(self):
		pygame.event.post(pygame.event.Event(pygame.QUIT, {}))

class MenuButton(Button):
	
	def __init__(self, position, mediator):
		super().__init__(menupic, position, mediator)
		
	def post(self):
		# halt game first? Should be a resume as well.
		pygame.event.post(pygame.event.Event(STOP, {'message':"menu"}))
