import pygame
from gameobj import Level, Wave, AttackerFactory, DefType, Melon
from gamectrl import MainController, EventHandler, GameWindow


# Creating a level
level1 = Level()
level1.set_board_size(4,6)

# Create waves
wave1 = Wave()
wave1.add(AttackerFactory, 5)
wave2 = Wave()
wave2.add(AttackerFactory, 10)

# Put wave in level
level1.add_wave(wave1)
level1.add_wave(wave2)

# Available defender types
level1.add_defender_type(DefType)
level1.add_defender_type(Melon)

# Main loop

# Creating a level
gossipgirl = EventHandler()
almighty = MainController([level1], gossipgirl)

while almighty.running:
	
	gossipgirl.sort_events()
	almighty.view.draw()
	# Instead of setting gameover variable, could empty and restore function list.
	if not almighty.asdf.gameover:
		for function in almighty.asdf.functions:
			function()
