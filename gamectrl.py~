import pygame, random, gameobj, sys

# EVENT TYPES
RELEASE = 25
ATT_RELEASE = 26
MOVE = 27
STOP = 31

move_interval = 500

class GameController:
	
	def __init__(self, level, gossipgirl):
		self.level = level
		# Nothing GC needs
		self.current_wave = self.level.next_wave()
		#
		
		self.event_handler = gossipgirl
		
		# All important sprite groups
		self.defenders = pygame.sprite.Group()
		self.attackers = pygame.sprite.Group()
		self.bullets = pygame.sprite.Group()
		self.resources = pygame.sprite.Group()
		self.def_types = pygame.sprite.Group()
		
		# Everything about coordinates
		self.lanes = self.level.lanes
		self.squares = self.level.squares
		self.tile_side = self.level.tile_side
		self.w_width = self.squares * self.tile_side
		self.w_height = self.lanes * self.tile_side
		self.board = gameobj.Board(self.lanes, self.squares)
		self.rand = Random(self.w_width, self.w_height)

		self.my_resource = gameobj.MyResource()
		
		# Nothing GC needs, just for convenience
		self.res_timer = RandomTimer(3, 4, gossipgirl, RELEASE, STOP)
		self.att_timer = RandomTimer(4, 6, gossipgirl, ATT_RELEASE, STOP)
		#
		# Generators
		self.resource_gen = ResourceGenerator(self.resources, self.rand, self.my_resource, self.res_timer, self.event_handler)
		self.attacker_gen = AttackerGenerator(self.attackers, self.current_wave, self.rand, self.w_width, self.att_timer, self.event_handler)
		
		# Periodic timers
		self.move_timer = pygame.time.set_timer(MOVE, move_interval)
		
		# View functions
		self.game_window = GameWindow((self.w_width, self.w_height))
		self.game_window.add_group(self.defenders)
		self.game_window.add_group(self.attackers)
		self.game_window.add_group(self.resources)
		self.game_window.add_group(self.bullets)
		self.game_window.add_group(self.def_types)
		
	
# Functions for placing game objects
	def tileCenter(self, lane, square):
		square -= 1
		lane -= 1
		tz = self.tile_side
		t = pygame.Rect(square * tz, lane * tz, tz, tz)
		return t.center

	def tileTopLeft(self, lane, square):
		square -= 1
		lane -= 1
		tz = self.tile_side
		t = pygame.Rect(square * tz, lane * tz, tz, tz)
		return t.center


# Creating defenders

	def create_defender(self, lane, square):
		unit = gameobj.Defender(self.tileCenter(lane, square))
		self.defenders.add(unit)
		self.board.place(lane, square)
		weapon = gameobj.Weapon(self.tileTopLeft(lane, square), self.tile_side, self.tileCenter(lane, square), self.bullets, self.event_handler)
		unit.set_weapon(weapon)


# Game logics

	def attacker_has_obstacle(self):
		for enemy in self.attackers:
			enemy.movement_now(self.defenders)
	
	def defender_detecting_enemy(self):
		for defender in self.defenders:
			defender.weapon.detection(self.attackers)
	
	def hit_attackers(self):
		for bullet, enemies in pygame.sprite.groupcollide(self.bullets, self.attackers, False, False).items():
			enemies[0].hit(bullet.damage)
			bullet.die()
		

class EventHandler:
	def __init__(self):
		# Make a list of events instead of putting event types manually each time add/remove a e-type
		self.subscribers = {pygame.MOUSEBUTTONUP:[], RELEASE:[], ATT_RELEASE:[], STOP:[], MOVE:[]}
		
	def add(self, subscriber, EvType):
		if EvType in self.subscribers:
			self.subscribers[EvType].append(subscriber)
		else:
			print('not able to add', EvType)
	
	def remove(self, subscriber):
		for sslist in self.subscribers.values():
			if subscriber in sslist:
				sslist.remove(subscriber)
		
	def sort_events(self):
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				sys.exit()
			elif event.type in self.subscribers:
				for subscriber in self.subscribers[event.type]:
					subscriber.notify(event)


class GameWindow:
	
	# Can add functions to change groups and backgroundpicture
	
	def __init__(self, size):
	#	pygame.display.set_icon(desktop_icon)
		pygame.display.set_caption('Test Game Window')
		self.screen = pygame.display.set_mode(size)
		self.bgcolor = 250, 250, 250
		self.groups = []
	
	def add_group(self, group):
		self.groups.append(group)
	
	def draw(self):		# Maybe become draw game and add method draw start screen
	#	for group in self.groups:
	#		group.update()
			
		self.screen.fill(self.bgcolor) # blit background image in future
		
		for group in self.groups:
			group.draw(self.screen)
		
		pygame.display.flip()

# Rename this class
class Random:
	def __init__(self, width, height):
		self.w = width
		self.h = height
	
	def coord(self, x=None, y=None):
		if not x:
			x = random.randint(0, self.w)
		if not y:
			y = random.randint(0, self.h)
		return x, y


class RandomTimer:
	def __init__(self, timer_min, timer_max, gossipgirl, postEtype, listenEtype):
		self.time = 0
		self.min = timer_min
		self.max = timer_max
		self.postEtype = postEtype
		self.mediator = gossipgirl
		self.mediator.add(self, listenEtype)
	
	def start(self):
		self.new_time()
		
	def stop(self):
		self.time = 0
		self.recall()
		
	def new_time(self):
		self.time = random.randint(self.min, self.max)*1000
		self.recall()
	
	def recall(self):
		timer = pygame.time.set_timer(self.postEtype, self.time)

	def notify(self, event):
		self.stop()
		

class AttackerGenerator:
	
	def __init__(self, attackers, wave, place, w_width, timer, mediator):
		self.attackers = attackers
		self.wave = wave
		self.place = place
		self.start = w_width
		self.timer = timer
		self.mediator = mediator
		self.timer.start()
		self.mediator.add(self, ATT_RELEASE)
	
	def throw_attacker(self):
		factory = self.wave.get_enemy()
		position = self.place.coord(x=self.start)
		self.attackers.add(factory.create(position, self.mediator))
	
	def notify(self, event):
		self.throw_attacker()
		self.timer.new_time()


class ResourceGenerator:
	
	def __init__(self, resources, place, counter, timer, mediator):
		self.resources = resources
		self.place = place
		self.counter = counter
		self.timer = timer
		self.mediator = mediator
		self.timer.start()
		self.mediator.add(self, RELEASE)
	
	def throw_resource(self):
		position = self.place.coord()
		self.resources.add(gameobj.Resource(position, self.counter, self.mediator))
	
	def notify(self, event):
		self.throw_resource()
