import pygame, random, gameobj, sys, gc
from globalstuff import *


class MainController:
	
	def __init__(self, levels, gossipgirl):
		
		# Quit program variable
		self.running = True
		
		# Subscribing events
		self.event_handler = gossipgirl
		self.event_handler.add(self, pygame.QUIT)
		self.event_handler.add(self, STOP)
		self.event_handler.add(self, START_GAME)
		
		# View
		self.positioner = Positioner()
		self.view = GameWindow((self.positioner.width, self.positioner.height))
		
		self.levels = levels
		
		# Objects of the state right now
		self.asdf = None
		self.groups = []
		
		# When initiating game, always load menu first.
		self.load_menu()
	
	def load_menu(self):
		self.asdf = MainMenu(self.event_handler)
		self.groups = [self.asdf.buttons]
		self.view_groups()
	
	def load_game(self, level):
		self.asdf = GameController(level, self.event_handler)
		self.groups = [self.asdf.def_types,
					   self.asdf.defenders,
					   self.asdf.attackers,
					   self.asdf.bullets,
					   self.asdf.buttons,
					   self.asdf.resources]
		self.view_groups()
	
	def unload(self):
	#	A way to kill everything in current state before loading next state.
	#	print('unloading')
	#	pygame.event.post(pygame.event.Event(UNSUBSCRIBE, {}))
	#	print('event sent')
		for group in self.groups:
			for item in group:
				item.mediator.remove(item)
	
	# Updating view each time state changes.
	def view_groups(self):
		# In future there might be more functionalities in here
		self.view.add_groups(self.groups)
	
	def notify(self, event):
		if event.type == pygame.QUIT:
			self.running = False
		if event.type == STOP:
			if event.message == "menu":
				self.unload()
				self.load_menu()
		elif event.type == START_GAME:
			self.unload()
			self.load_game(self.levels[event.level])

	
class EventHandler:
	
	def __init__(self):
		# Make a list of events instead of putting event types manually each time add/remove a e-type
		self.subscribers = {pygame.QUIT:[],
							pygame.MOUSEBUTTONUP:[],
							pygame.MOUSEBUTTONDOWN:[],
							RELEASE:[],
							ATT_RELEASE:[],
							STOP:[],
							MOVE:[],
							DEF_REMOVE:[],
							TICK:[],
							START_GAME:[]}
		
	def add(self, subscriber, EvType):
		if EvType in self.subscribers:
			self.subscribers[EvType].append(subscriber)
		else:
			print('not able to add', EvType)
	
	def remove(self, subscriber):
		for sslist in self.subscribers.values():
			if subscriber in sslist:
				sslist.remove(subscriber)
				print(subscriber, "removed.")
	
	def view(self, subscriber):
		for event, sslist in self.subscribers.items():
			if subscriber in sslist:
				print('Event', event, sslist)
	
	def sort_events(self):
		for event in pygame.event.get():
			if event.type in self.subscribers:
				for subscriber in self.subscribers[event.type]:
					subscriber.notify(event)


class GameWindow:
	
	# Can add functions to change groups and background image
	
	def __init__(self, size):
		pygame.display.set_icon(desktop_icon)
		pygame.display.set_caption('Game Window')
		self.screen = pygame.display.set_mode(size)
		self.bgcolor = 255, 255, 255
		self.bgimage = None
		self.groups = []
		self.functions = []
	
	def add_groups(self, groups):
		self.groups = groups

	def draw(self):
		if self.bgimage:
			print('please write the function to set background picture.')
		else:
			self.screen.fill(self.bgcolor)
		
		for group in self.groups:
			group.draw(self.screen)
		
		pygame.display.flip()


class GameController:
	
	def __init__(self, level, mediator):
		
		# Utilities/necessities
		self.level = level
		self.positioner = Positioner()
		self.mediator = mediator
		self.mediator.add(self, STOP)
		self.gameover = False
		
		# All sprite groups
		self.def_types = pygame.sprite.Group()
		self.defenders = pygame.sprite.Group()
		self.attackers = pygame.sprite.Group()
		self.bullets = pygame.sprite.Group()
		self.buttons = pygame.sprite.Group()
		self.resources = pygame.sprite.Group()
		
		# Create and place menu and restart buttons
		self.buttons.add(gameobj.MenuButton((690, 25), self.mediator))
		
		# Some objects
		self.board = gameobj.Board(self.positioner.lanes, self.positioner.squares, self.mediator)
		self.my_resource = gameobj.MyResource()
		self.house = gameobj.House()
		
		# Defender Placer with available defender types
		self.defplacer = DefenderPlacer(self.defenders, self.bullets, self.positioner, self.board, self.mediator)
		self.defplacer.add(self.def_types)
		self.create_def_types()
		
		# Generators
		self.resource_gen = ResourceGenerator(self.resources, self.positioner, self.my_resource, self.mediator)
		self.attacker_gen = AttackerGenerator(self.attackers, self.level.waves, self.positioner, self.mediator)
		
		# Timer. In future just one timer. Each object will count time self
		self.move_timer = pygame.time.set_timer(MOVE, move_interval)
		
		# Game logic
		self.functions = [self.attacker_has_obstacle,
						  self.defender_detecting_enemy,
						  self.hit_attackers,
						  self.enemies_won,
						  self.defplacer.update]
		
		
	# Create defender types. Needs to be rewritten to add more than one defender type.
	def create_def_types(self):
		# defender type refers to defplacer only for a function. Maybe communicate via event or just give the callback function.
		for i, dtype in enumerate(self.level.def_types.keys()):
			self.def_types.add(dtype(self.positioner.tile_center(i+1, 0), self.defplacer, self.mediator))
	
	# Game logics (To be tested for every cpu-tick) Can be initiated by receiving cpu tick events instead.
	def attacker_has_obstacle(self):
		for enemy in self.attackers:
			enemy.movement_now(self.defenders)
	
	def defender_detecting_enemy(self):
		for defender in self.defenders:
			defender.weapon.detection(self.attackers)
	
	def hit_attackers(self):
		for bullet, enemies in pygame.sprite.groupcollide(self.bullets, self.attackers, False, False).items():
			enemies[0].hit(bullet.damage)
			bullet.die()
	
	def defeated_enemies(self):
		if not self.attackers:
			print("self.attackers", "You won, level passed.")
			self.halt_game()
	
	def enemies_won(self):
		if pygame.sprite.spritecollideany(self.house, self.attackers):
			print("You lose!")
			self.halt_game()
	
	# Stop a game
	def halt_game(self):
		pygame.event.post(pygame.event.Event(STOP, {"message":"game finished"}))
		# When implemented a speed class the speed class with subscribe game finished.
		move_interval = 0
		self.move_timer = pygame.time.set_timer(MOVE, move_interval)
		self.gameover = True
		
	def continue_game(self):
		pass
	
	# Receive notifications about pausing and resuming game
	def notify(self, event):
		print(self, event.type, event.message)
		if event.message == "no more enemies":
			self.functions.append(self.defeated_enemies)
			print(self.functions)

	
class MainMenu:
	def __init__(self, mediator):
		self.buttons = pygame.sprite.Group()
		self.functions = []
		self.gameover = False
		self.mediator = mediator
		self.buttons.add(gameobj.LevelButton(0, (720/3, 460/2), self.mediator))
		self.buttons.add(gameobj.ExitButton((720/3*2, 460/2), self.mediator))
		for button in self.buttons:
			print(gc.get_referrers(button))


class RandomTimer:
	def __init__(self, timer_min, timer_max, postEtype):
		self.time = 0
		self.min = timer_min
		self.max = timer_max
		self.postEtype = postEtype		
	
	def start(self):
		self.new_time()
		print("timer started")
		
	def stop(self):
		self.time = 0
		self.recall()
		print("timer stopped")
		
	def new_time(self):
		self.time = random.randint(self.min, self.max)*1000
		self.recall()
	
	def recall(self):
		timer = pygame.time.set_timer(self.postEtype, self.time)


class Positioner:
	def __init__(self):
		self.lanes = 4
		self.squares = 6
		self.tile_side = 100
		self.card_size = "This decides the card_bar"
		self.card_bar = 120
		self.status_bar = 60
		self.width = self.card_bar + self.squares * self.tile_side
		self.height = self.status_bar + self.lanes * self.tile_side
		print(self.width, self.height)
	
	def lane_center(self, lane):
		return int(self.status_bar + (lane - 0.5) * self.tile_side)
		
	def lane_top(self, lane):
		return int(self.status_bar + (lane - 1) * self.tile_side)
		
	def lane_bottom(self, lane):
		return int(self.status_bar + lane * self.tile_side)
	
	def square_center(self, square):
		return int(self.card_bar + (square - 0.5) * self.tile_side)
	
	def square_left(self, square):
		return int(self.card_bar + (square - 1) * self.tile_side)
	
	def square_right(self, square):
		return int(self.card_bar + square * self.tile_side)
	
	def random_lane(self, x):
		return x, self.lane_center(random.randint(1, self.lanes))
		
	def random_square(self, y):
		return pos
	
	def random_place(self):
		return random.randint(0, self.width), random.randint(0, self.height)
	
	def tile_center(self, lane, square):
		return self.square_center(square), self.lane_center(lane)
	
	def tile_top_left(self, lane, square):
		return self.square_left(square), self.lane_top(lane)
	
	def return_tile(self, point):
		x, y = point
		lane = (y - self.status_bar) // self.tile_side + 1
		square = (x - self.card_bar) // self.tile_side + 1
		if 1 <= lane <= self.lanes and 1 <= square <= self.squares:
			return lane, square
		else:
			return None, None
	
	def off_screen(self, rect):
		if rect.left > self.width or rect.right < 0 or rect.bottom < 0 or rect.top > self.height:
			return True
		else:
			return False
	
	def range(self, lane, square, area):
		rect = pygame.Rect(self.tile_top_left(lane, square), (min(area, self.squares-square + 1)*self.tile_side, self.tile_side))
		print(rect.left, rect.right)
		return rect
		
	def muzzle_pos(self, lane, square, x, y):
		return x + self.card_bar + (square - 1) * 100, y + self.status_bar + (lane - 1) * 100


class AttackerGenerator:
	
	def __init__(self, attackers, waves, positioner, mediator):
		self.attackers = attackers
		self.waves = waves
		self.current_wave = None
		self.positioner = positioner
		self.timer = self.att_timer = RandomTimer(4, 6, ATT_RELEASE)
		self.mediator = mediator
		self.mediator.add(self, ATT_RELEASE)
		self.mediator.add(self, STOP)
		self.start_wave()
	
	def start_wave(self):
		self.current_wave = self.waves[0]
		print(self.current_wave)
		# set a pre wave start timer. having timer.start as callback.
		self.timer.start()
		
	def remove_wave(self):
		self.timer.stop()
		self.waves.remove(self.current_wave)
		if self.waves:
			self.start_wave()
		else:
			print(self, "posting no more enemies")
			pygame.event.post(pygame.event.Event(STOP, {"message": "no more enemies"}))
		
	def throw_attacker(self):
		factory = self.current_wave.get_enemy()
		position = self.positioner.random_lane(self.positioner.square_left(7))
		self.attackers.add(factory.create(position, self.mediator))
		self.timer.new_time()

	def notify(self, event):
		if event.type == ATT_RELEASE:
			self.throw_attacker()
		elif event.type == STOP:
			print(self, event.type, event.message)
			if event.message == "wave finished":
				self.remove_wave()
			elif event.message == "game finished":
				self.timer.stop()

		
class ResourceGenerator:
	
	def __init__(self, resources, positioner, counter, mediator):
		self.resources = resources
		self.positioner = positioner
		self.counter = counter
		self.timer = RandomTimer(3, 4, RELEASE)
		self.mediator = mediator
		self.timer.start()
		self.mediator.add(self, RELEASE)
		self.mediator.add(self, STOP)
	
	def throw_resource(self):
		position = self.positioner.random_place()
		self.resources.add(gameobj.Resource(position, self.counter, self.mediator))
	
	def notify(self, event):
		if event.type == RELEASE:
			self.throw_resource()
		elif event.type == STOP:
			print(self, event.type, event.message)
			if event.message == "game finished":
				self.timer.stop()
		

class DefenderPlacer(pygame.sprite.Sprite):
	
	def __init__(self, defenders, bullets, positioner, board, mediator):
		pygame.sprite.Sprite.__init__(self)
		self.hide = pygame.Surface((1,1))
		self.image = self.hide
		self.rect = pygame.Rect(0, 0, 100, 100)
		self.show = False
		self.defenders = defenders
		self.bullets = bullets
		self.defender = None
		self.positioner = positioner
		self.board = board
		self.mediator = mediator
		self.mediator.add(self, pygame.MOUSEBUTTONUP)
	
	def activate(self, image, defender):
		self.image = image
		self.defender = defender
		self.show = True
	
	def place_defender(self, lane, square):
		if self.defender == None:
			print("No defender!!")
			return
		if lane == None:
			self.defender = None
			return
		if self.board.occupied(lane, square):
			print("Pity, occupied.")
			self.defender = None
			return
		self.defenders.add(self.defender)
		self.board.occupy(self.defender, lane, square)
		weapon = gameobj.Weapon(lane, square, self.bullets, self.positioner, self.mediator)
		self.defender.set_position(self.positioner.tile_center(lane, square))
		self.defender.set_weapon(weapon)
		self.defender = None
	
	def update(self):
		if self.show:
			lane, square = self.positioner.return_tile(pygame.mouse.get_pos())
			if lane:
				self.rect = self.positioner.tile_top_left(lane, square)
			else:
				self.rect = self.positioner.tile_top_left(-200, -200)
	
	def notify(self, event):
		if self.show:
			lane, square = self.positioner.return_tile(pygame.mouse.get_pos())
			self.place_defender(lane, square)
			self.image = self.hide
			self.show = False
			
